package focmb.keepscreenawake;

import android.widget.Button;
import focmb.keepscreenawake.R;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class AwakeWidgetProvider extends AppWidgetProvider
{

    public static String WIDGET_BUTTON = "focmb.keepscreenawake.WIDGET_BUTTON";

    @Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
    {
    	// TODO Auto-generated method stub
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),	R.layout.awake_widget);
		//remoteViews.setOnClickPendingIntent(R.id.widget_button, buildButtonPendingIntent(context));

        Intent intent = new Intent(WIDGET_BUTTON);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.widget_button, pendingIntent );

        pushWidgetUpdate(context, remoteViews);




	}

	public static void pushWidgetUpdate(Context context, RemoteViews remoteViews)
    {
		// TODO Auto-generated method stub
		ComponentName myWidget = new ComponentName(context,	AwakeWidgetProvider.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(context);
		manager.updateAppWidget(myWidget, remoteViews);

	}

    /*
	public static PendingIntent buildButtonPendingIntent(Context context)
    {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setAction("focmb.keepscreenawake.WIDGET_BUTTON");

		return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
    */
}
