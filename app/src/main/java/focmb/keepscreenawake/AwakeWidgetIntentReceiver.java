package focmb.keepscreenawake;

import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

//TODO: Timeout auf Buttonclick ausschalten

public class AwakeWidgetIntentReceiver extends BroadcastReceiver
{

	public static String WIDGET_BUTTON = "focmb.keepscreenawake.WIDGET_BUTTON";

	@Override
	public void onReceive(Context context, Intent intent)
    {
        if (WIDGET_BUTTON.equals(intent.getAction()))
        {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl =  pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK , "My Tag");
            wl.acquire();
            Toast.makeText(context.getApplicationContext(), "Wake Lock an", Toast.LENGTH_SHORT).show();
            //wl.release();
        }

	}

	/*
    private void updateWidgetPictureAndButtonListener(Context context)
    {
		// TODO Auto-generated method stub
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.awake_widget);
		remoteViews.setImageViewResource(R.id.widget_image, getImageToSet());
		
		remoteViews.setOnClickPendingIntent(R.id.widget_button, AwakeWidgetProvider.buildButtonPendingIntent(context));
		AwakeWidgetProvider.pushWidgetUpdate(context, remoteViews);
		
	}

	private int getImageToSet() {
		// TODO Auto-generated method stub
		clickCount ++;
        return clickCount % 2 == 0 ? R.drawable.me : R.drawable.wordpress_icon;
	}
	*/
}
